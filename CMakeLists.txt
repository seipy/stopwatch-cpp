cmake_minimum_required(VERSION 3.0)
project(stopwatch-cpp CXX)

find_package(Curses REQUIRED)
include_directories(${CURSES_INCLUDE_DIR})

add_executable(stopwatch-cpp stopwatch.cpp)
target_link_libraries(stopwatch-cpp ${CURSES_LIBRARIES})
